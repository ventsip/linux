package main

import (
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

func session(c net.Conn, wg *sync.WaitGroup) {
	defer wg.Done()
	defer c.Close()

	io.Copy(os.Stdout, c)
	log.Println("- Closing")
}

func serve(listener net.Listener, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		conn, err := listener.Accept()

		if err != nil {
			log.Println(err)
			break
		}

		log.Println("- Accepted connection local:", conn.LocalAddr(), "remote:", conn.RemoteAddr())

		wg.Add(1)
		go session(conn, wg)
	}
}

func wgWaitChannel(wg *sync.WaitGroup) <-chan struct{} {
	wgc := make(chan struct{})
	go func() {
		wg.Wait()
		close(wgc)
	}()
	return wgc
}

func main() {

	// open unix socket
	socketName := "./socket.sock"
	os.Remove(socketName)
	listener, err := net.Listen("unix", socketName)

	if err != nil {
		log.Println(err)
		return
	}
	defer os.Remove(socketName)

	log.Println("- Listening on", listener.Addr())

	var wg sync.WaitGroup
	wg.Add(1)
	go serve(listener, &wg)

	time.Sleep(time.Second * 10)
	listener.Close()

	select {
	case <-wgWaitChannel(&wg):
		log.Println("- All sessions closed")
	case <-time.After(time.Second * 10):
		log.Println("- Some sessions forcefully closed after timeout")
	}

	return
}
